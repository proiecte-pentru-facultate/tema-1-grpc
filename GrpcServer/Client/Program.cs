﻿using System;
using Grpc.Net.Client;
using System.Threading.Tasks;
using GrpcServer;
using System.Collections.Generic;

namespace Client
{
    class Program
    {

        private static void readTheUsers(List<UserInfo> listOfUsers,int numberOfUsers)
        {

            for(var i = 0;i<numberOfUsers;++i)
            {
                UserInfo user = new UserInfo();

                Console.WriteLine("Introduceti numele : ");
                user.Name = Console.ReadLine();

                Console.WriteLine("Introduceti CNP : ");
                user.UserID = Console.ReadLine();

                listOfUsers.Add(user);

            }
        }

        static void Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var userOneInfo = new UserInfo();

            int numberOfUsers = 0;

            List<UserInfo> listOfUsers = new List<UserInfo>();

            var userClient = new User.UserClient(channel);

            Console.WriteLine("Se citeset numarul de useri : ");

            numberOfUsers = Convert.ToInt32(Console.ReadLine());

            readTheUsers(listOfUsers, numberOfUsers);


            for(var i = 0;i<numberOfUsers;++i)
            {
                var user = userClient.GetClientInformations(listOfUsers[i]);

                Console.WriteLine($"{user.Name} are {user.Age} si este de genul {user.Gender}");

            }

            


            Console.ReadLine();

         

        }
    }
}
