﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;


namespace GrpcServer.Services
{
    public class UserService : User.UserBase
    {
        private readonly int numberOfDigits;
        private readonly ILogger<UserService> _logger;

        public UserService(ILogger<UserService> logger)
        {
            _logger = logger;
            numberOfDigits = 13;
        }


        public override Task<UserResponseInfo> GetClientInformations(UserInfo request, ServerCallContext context)
        {
            UserResponseInfo userInformations = new UserResponseInfo();


            if (request.UserID.Length < numberOfDigits) throw new ArgumentException("The userID is not valid!");

            userInformations.Gender = GetTheGender(request.UserID);
            userInformations.Name = request.Name;
            userInformations.Age = CalculateTheAge(request.UserID);


            return Task.FromResult(userInformations);

        }


        private string GetTheGender(string userID)
        {
            return userID.Substring(0, 1) == "1" ? "M" : "F";
        }

        private int CalculateTheAge(string userID)
        {
            int currentYear = DateTime.Now.Year;

            string yearOfBirth = "19" + userID.Substring(1, 2);

            return currentYear - Convert.ToInt32(yearOfBirth);
        }
    }
}
